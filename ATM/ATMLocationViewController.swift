//
//  ATMLocationViewController.swift
//  ATM
//
//  Created by Ryan Smith on 9/27/17.
//  Copyright © 2017 indiePixel. All rights reserved.
//

import UIKit

class ATMLocationViewController: UIViewController {
    
    var stackView: UIStackView!
    var location: ATMLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        // add a stack view inside a scroll view
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = UIColor.clear
        view.addSubview(scrollView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[scroll]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["scroll": scrollView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[scroll]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["scroll": scrollView]))
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.distribution = .fill
        stackView.spacing = 8
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.addSubview(stackView)
        
        // subviews of scrollviews require special autolayout treatment...
        view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: .top, relatedBy: .equal, toItem: stackView, attribute: .top, multiplier: 1.0, constant: -16))
        view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: .bottom, relatedBy: .equal, toItem: stackView, attribute: .bottom, multiplier: 1.0, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: .leading, relatedBy: .equal, toItem: stackView, attribute: .leading, multiplier: 1.0, constant: -16))
        view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: .trailing, relatedBy: .equal, toItem: stackView, attribute: .trailing, multiplier: 1.0, constant: -16))
        view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: .width, relatedBy: .equal, toItem: stackView, attribute: .width, multiplier: 1.0, constant: 32))
        
        addLocationInfo()
    }
    
    // adds stackviews containing the data for a location to the main stackview in a flexible manner.
    func addLocationInfo() {
        if let location = location {
            if let bank = location.bank {
                var bankAndLocationType = bank
                if let locType = location.locType {
                    if locType.lowercased() == "atm" {
                        bankAndLocationType += " ATM"
                    }
                    else if locType.lowercased() == "branch" {
                        bankAndLocationType += " Branch"
                    }
                }
                stackView.addOptionalArrangedSubview(label(forValue: bankAndLocationType, textStyle: .title1))
            }
            stackView.addOptionalArrangedSubview(label(forValue: location.title, textStyle: .title2))
            
            if let address = location.address, let cityStateZip = location.cityStateZip {
                stackView.addOptionalArrangedSubview(section(forValues: [address, cityStateZip], sectionName: "Address"))
            }
            
            stackView.addOptionalArrangedSubview(section(forValue: location.phone, sectionName: "Phone"))
            
            if let distance = location.distance {
                stackView.addOptionalArrangedSubview(section(forValue: "\(distance) miles", sectionName: "Distance"))
            }
            
            if let atms = location.atms {
                stackView.addOptionalArrangedSubview(section(forValue: "\(atms)", sectionName: "ATMs"))
            }
            
            if location.hasLobbyHours() {
                stackView.addOptionalArrangedSubview(section(forValues: location.labelWithDays(location.lobbyHours), sectionName: "Lobby Hours"))
            }
            
            if location.hasDriveUpHours() {
                stackView.addOptionalArrangedSubview(section(forValues: location.labelWithDays(location.driveUpHours), sectionName: "Drive Up Hours"))
            }
            
            stackView.addOptionalArrangedSubview(section(forValues: location.services, sectionName: "Services"))
            
            stackView.addOptionalArrangedSubview(section(forValue: location.type, sectionName: "Type"))
            divideView()
        }
    }
    
    func label(forValue value: String?, textStyle: UIFontTextStyle = .body, color: UIColor = .black) -> UILabel? {
        guard let value = value else { return nil }
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = UIColor.clear
        label.textColor = color
        label.font = UIFont.preferredFont(forTextStyle: textStyle)
        label.numberOfLines = 0
        label.text = value
        
        return label
    }
    
    func heading(withTitle title: String) -> UILabel? {
        let heading = label(forValue: "\(title)", textStyle: .headline)
        // without lowering the compression resistance priority, the non-heading labels sometimes get scrunched.
        heading?.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return heading
    }
    
    // stackview with heading on left side, value on right side
    func section(forValue value: String?, sectionName: String? = nil) -> UIStackView? {
        guard let value = value else { return nil }
        divideView()
        let sectionStackView = UIStackView()
        sectionStackView.axis = .horizontal
        sectionStackView.alignment = .top
        sectionStackView.distribution = .fill
        sectionStackView.spacing = 8
        
        if let sectionName = sectionName {
            let headingStackView = UIStackView()
            headingStackView.axis = .vertical
            headingStackView.alignment = .leading
            headingStackView.distribution = .fill
            headingStackView.spacing = 8
            
            headingStackView.addOptionalArrangedSubview(heading(withTitle: sectionName))
            sectionStackView.addOptionalArrangedSubview(headingStackView)
        }
        
        let valueStackView = UIStackView()
        valueStackView.axis = .vertical
        valueStackView.alignment = .trailing
        valueStackView.distribution = .fill
        valueStackView.spacing = 8
        valueStackView.addOptionalArrangedSubview(label(forValue: value))
        
        sectionStackView.addOptionalArrangedSubview(valueStackView)
        
        return sectionStackView
    }
    
    // stackview with heading on left side, 'rows' of values on the right side
    func section(forValues values: [String]?, sectionName: String? = nil) -> UIStackView? {
        guard let values = values, values.count > 0 else { return nil }
        divideView()
        let sectionStackView = UIStackView()
        sectionStackView.axis = .horizontal
        sectionStackView.alignment = .top
        sectionStackView.distribution = .fill
        sectionStackView.spacing = 8
        
        if let sectionName = sectionName {
            let headingStackView = UIStackView()
            headingStackView.axis = .vertical
            headingStackView.alignment = .leading
            headingStackView.distribution = .fill
            headingStackView.spacing = 8
            
            headingStackView.addOptionalArrangedSubview(heading(withTitle: sectionName))
            sectionStackView.addOptionalArrangedSubview(headingStackView)
        }
        
        let valueStackView = UIStackView()
        valueStackView.axis = .vertical
        valueStackView.alignment = .trailing
        valueStackView.distribution = .fill
        valueStackView.spacing = 4
        
        for value in values {
            valueStackView.addOptionalArrangedSubview(label(forValue: value))
        }
        
        sectionStackView.addOptionalArrangedSubview(valueStackView)
        
        return sectionStackView
    }
    
    // adds a divider line to the stackview
    func divideView() {
        let dividerView = UIView()
        dividerView.translatesAutoresizingMaskIntoConstraints = false
        dividerView.backgroundColor = UIColor(white: 0, alpha: 0.1)
        dividerView.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        stackView.addOptionalArrangedSubview(dividerView)
        dividerView.widthAnchor.constraint(equalTo: stackView.widthAnchor, multiplier: 1.0).isActive = true
    }
}

extension UIStackView {
    func addOptionalArrangedSubview(_ view: UIView?) {
        if let view = view {
            addArrangedSubview(view)
        }
    }
}
