//
//  MapViewController.swift
//  ATM
//
//  Created by Ryan Smith on 9/27/17.
//  Copyright © 2017 indiePixel. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class MapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    let locationManager = CLLocationManager()
    var mapView: GMSMapView?
    var userMarker: GMSMarker?
    var userCircle: GMSCircle?
    
    var locations: [ATMLocation] = []
    var markers: [GMSMarker] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "ATM Locations"
        locationManager.delegate = self
        locationManager.distanceFilter = 10 // ignore location changes less than this value in meters
        locationManager.startUpdatingLocation()
    }
    
    func updateMapView(withLocation location: CLLocation) {
        if let mapView = mapView {
            mapView.moveCamera(GMSCameraUpdate.setTarget(location.coordinate))
        }
        else {
            let camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 14.0)
            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            mapView.translatesAutoresizingMaskIntoConstraints = false
            mapView.delegate = self
            view.addSubview(mapView)
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[map]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["map": mapView]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[map]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["map": mapView]))
            self.mapView = mapView
        }
        updateUserMarker(withLocation: location)
    }
    
    func updateUserMarker(withLocation location: CLLocation) {
        if let userMarker = userMarker {
            userMarker.position = location.coordinate
        }
        else {
            let marker = GMSMarker()
            marker.position = location.coordinate
            marker.title = "User"
            marker.icon = GMSMarker.markerImage(with: UIColor(red: 0.2, green: 0.2, blue: 0.9, alpha: 1.0))
            marker.map = mapView
            userMarker = marker
        }
        
        // add accuracy radius indication
        if let userCircle = userCircle {
            userCircle.position = location.coordinate
            userCircle.radius = location.horizontalAccuracy
        }
        else {
            let circleCenter = location.coordinate
            let circle = GMSCircle(position: circleCenter, radius: location.horizontalAccuracy)
            
            circle.fillColor = UIColor(red: 0.2, green: 0.2, blue: 0.9, alpha: 0.05)
            circle.strokeColor = UIColor(red: 0.2, green: 0.2, blue: 0.9, alpha: 0.75)
            circle.strokeWidth = 1
            circle.map = mapView
            userCircle = circle
        }
    }
    
    func updateMarkers() {
        // remove existing markers from map
        for marker in markers {
            marker.map = nil
        }
        markers.removeAll()
        
        // add marker for each location
        if locations.count > 0 {
            for location in locations {
                let marker = GMSMarker()
                marker.position = location.coordinate
                marker.title = location.title
                marker.map = mapView
                markers.append(marker)
            }
        }
    }

    //MARK: - CLLocationManagerDelegate medthods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let userLocation = locations.first {
            // user has moved, so update the map view to reflect this change
            updateMapView(withLocation: userLocation)
            // or sometimes just the horizontal accuracy has changed and that triggers this?
        }
    }
    
    //MARK: - GMSMapViewDelegate methods
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        // when map stops, get center coordinate and fire off request for location data
        let parameters = ["lat": position.target.latitude, "lng": position.target.longitude]
        
        // I don't like having the http request here in the view controller, but I put it here because there's just the one, simple call.
        // If there was a full api with options, I'd create a 'router' enum...
        Alamofire.request("https://m.chase.com/PSRWeb/location/list.action", parameters: parameters).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                self.locations.removeAll()
                
                let json = JSON(value)
                
                if let jsonLocations = json["locations"].array {
                    // create an ATMLocation for each location returned
                    for jsonLocation in jsonLocations {
                        self.locations.append(ATMLocation(withJSON: jsonLocation))
                    }
                    self.updateMarkers()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let index = markers.index(of: marker) else { return false }
        let locationVC = ATMLocationViewController()
        locationVC.location = locations[index]
        navigationController?.pushViewController(locationVC, animated: true)
        return true
    }
}
