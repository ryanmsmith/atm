//
//  ATMLocation.swift
//  ATM
//
//  Created by Ryan Smith on 9/26/17.
//  Copyright © 2017 indiePixel. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON

class ATMLocation: NSObject, MKAnnotation { // adopting the MKAnnotation protocol allows this to be easily used with MapKit...
    let state: String?
    let locType: String?
    let title: String? // MKAnnotation protocol
    let subtitle: String? // MKAnnotation protocol
    let address: String?
    let city: String?
    let zip: String?
    let name: String?
    let bank: String?
    let type: String?
    let phone: String?
    let atms: Int?
    let distance: Double?
    let access: String?
    let lobbyHours: [String]?
    let driveUpHours: [String]?
    let services: [String]?
    let coordinate: CLLocationCoordinate2D // MKAnnotation protocol
    
    init(withJSON json: JSON) {
        state = json["state"].string
        locType = json["locType"].string
        title = json["label"].string
        address = json["address"].string
        city = json["city"].string
        zip = json["zip"].string
        name = json["name"].string
        bank = json["bank"].string
        type = json["type"].string
        phone = json["phone"].string
        atms = json["atms"].int
        distance = json["distance"].double
        access = json["access"].string
        lobbyHours = json["lobbyHrs"].arrayObject as? [String]
        driveUpHours = json["driveUpHrs"].arrayObject as? [String]
        services = json["services"].arrayObject as? [String]
        subtitle = nil
        
        if let latitudeString = json["lat"].string, let longitudeString = json["lng"].string, let latitude = Double(latitudeString), let longitude = Double(longitudeString) {
            coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        else {
            coordinate = CLLocationCoordinate2D()
        }
    }
    
    var cityStateZip: String? {
        guard let city = city, let state = state, let zip = zip else {
            return nil
        }
        return "\(city), \(state) \(zip)"
    }
    
    func hasLobbyHours() -> Bool {
        guard let lobbyHours = lobbyHours else { return false }
        let testString = lobbyHours.reduce("", +)
        return testString != ""
    }
    
    func hasDriveUpHours() -> Bool {
        guard let driveUpHours = driveUpHours else { return false }
        let testString = driveUpHours.reduce("", +)
        return testString != ""
    }
    
    func labelWithDays(_ hours: [String]?) -> [String]? {
        guard let hours = hours else { return nil }
        let days = ["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat"]
        var labeledHours: [String] = []
        for (index, dayHours) in hours.enumerated() {
            if dayHours == "" {
                labeledHours.append(days[index] + " Closed")
            }
            else {
                labeledHours.append(days[index] + " " + dayHours)
            }
        }
        if labeledHours.count > 0 {
            return labeledHours
        }
        return nil
    }
}
